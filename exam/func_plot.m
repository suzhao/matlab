fplot(@(x) x.^2);
hold on;
fplot(@(x) exp(x));
hold on;
fplot(@(x) log(x));
hold on;
fplot(@(x) sin(x));
hold on;
fplot(@(x) cos(x));
axis equal;
axis([-10 10 -6 6]);
xlabel('x');
ylabel('y');
title('基本初等函数图象');
legend('x^2','e^x','ln(x)','sin(x)','cos(x)')