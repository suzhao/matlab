function [Q]=intquad(s)
Z=ones(s);
Q=[Z*-1,Z*exp(1);Z*pi,Z];
end

