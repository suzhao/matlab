function s=diag_sum(U)
[s,z]=size(U);
h=min(s,z);
o=(a+1)/2;
U=U([1:h],[1:h]);
S=flip(U);
if mod(h,2)==0
    s=sum(diag(U))+sum(diag(S));
else
    s=sum(diag(U))+sum(diag(S))-U(o,o);
end

